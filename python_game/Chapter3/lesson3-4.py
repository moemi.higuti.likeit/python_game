# pop up some numbers of 1 to 4
for i in range(1,5):
    print(i)

print("")
for i in range (10):
    print(i)

print("")
for i in range (10, 0, -2):
    print(i)

print("")
i=0
while i < 5:
    print(i)
    i=i+1

print("")
def win():
    print("you won")

#you need to type in win() if you wanna to run win() method
win()

print("")
def recover(val):
    print("your hp : ")
    print(val)
    print("recovery")

recover(100)

print("")
def add(a,b):
    return a+b

c = add(1,2)
print(c)
